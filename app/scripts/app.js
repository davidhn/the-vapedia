'use strict';

/**
 * @ngdoc overview
 * @name thevapediaApp
 * @description
 * # thevapediaApp
 *
 * Main module of the application.
 */
var app = angular
  .module('thevapediaApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'firebase'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/vapes.html',
        controller: 'VapeCtrl'
      })
      .when('/vapes/:vapeId', {
        templateUrl: 'views/showvape.html',
        controller: 'VapeViewCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
  .constant('FIREBASE_URL', 'https://the-vapedia.firebaseio.com/');
