'use strict';

app.factory('Vape', function($firebase, FIREBASE_URL) {
	var ref = new Firebase(FIREBASE_URL);
	var vapes = $firebase(ref.child('vapes')).$asArray();

	var Vape = {
		all: vapes,
		create: function (vape) {
			return vapes.$add(vape);
		},
		get: function (vapeId) {
			return $firebase(ref.child('vapes').child(vapeId)).$asObject();
		},
		delete: function (vape) {
			return vapes.$remove(vape);
		}
	};

	return Vape;

});