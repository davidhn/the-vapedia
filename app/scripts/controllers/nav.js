'use strict';

app.controller('NavCtrl', function ($scope, $location, Vape) {
	//create an empty mod array
	$scope.vapes = Vape.all;
	console.log($scope.vapes);

	//push UNIQUE brand names into array

	
	$scope.vape = {
		brand: '',
		name: '',
		type: '',
		image: '',
		country: '',
		url: '',
	};

	$scope.submitVape = function () {
		Vape.create($scope.vape).then(function (ref) {
			$location.path('/vapes/' + ref.name());
			$scope.vape = {
				brand: '',
				name: '',
				type: '',
				image: '',
				country: '',
				url: '',
			};
		});
	};


})