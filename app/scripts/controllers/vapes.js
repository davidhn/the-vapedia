'use strict';

app.controller('VapeCtrl', function ($scope, $location, Vape) {
	$scope.vapes = Vape.all;

	//////THIS IS ALREADY IN THE NAV//////
	$scope.vape = {
		brand: '',
		name: '',
		type: '',
		image: '',
		country: '',
		url: '',
	};

	$scope.submitVape = function(){
		Vape.create($scope.vape).then(function (ref) {
			$location.path('/vapes/' + ref.name());
			$scope.vape = {
				brand: '',
				name: '',
				type: '',
				image: '',
				country: '',
				url: '',
			};
		});
	};
	/////ALREADY IN THE NAV/////////////

	$scope.deleteVape = function(vape) {
		Vape.delete(vape);
	};

});