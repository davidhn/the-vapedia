'use strict';

app.controller('VapeViewCtrl', function ($scope, $routeParams, Vape) {
	$scope.vape = Vape.get($routeParams.vapeId)

	$scope.vapes = Vape.all;
})